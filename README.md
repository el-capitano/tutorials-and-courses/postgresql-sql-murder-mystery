
# SQL Murder Mystery, converted to PostgreSQL and Docker

This repository contains the necessary Docker Compose configuration to play the [SQL Murder Mystery](https://mystery.knightlab.com/) locally, in PostgreSQL.

[TOC]

## Repository contents

The repository contains a Docker Compose file which has a postgres and PGAdmin service for easy querying. In addition there is a short-lived PGLoader task which downloads the SQLite file from the SQL murder mystery repo and converts and loads the data into the Postgres service. 

The SQLite source may be changed with de `SQLITE_SOURCE_PATH` variable in the [Docker Compose file](./docker-compose.yaml).

## Get started

### Prerequisites

* `docker`
* `docker-compose`

### setup

* In this folder, start up the `docker-compose` services by issing the command: `docker-compose`
* Wait until the `pg_loader` is done loading the data (you'll see `pg_loader exited with code 0`)
* In your browser, navigate to `http://localhost:5050`. You'll see the PGAdmin login screen. Enter email `admin@admin.com` with password `root`.
* Create a new server with:
	* hostname: `postgres`
	* username: `mistery_root`
	* password: `mistery`

You now should be able to connect to the database and issue queries.

## Guided path to the solution of the SQL Murder Mystery

We are unfamiliar with this database, so first lets describe all the tables in the current database:

```sql
SELECT 
   table_name, 
   column_name, 
   data_type 
FROM 
   information_schema.columns
WHERE 
   table_name IN (
	   'crime_scene_report', 
	   'drivers_license', 
	   'facebook_event_checkin', 
	   'get_fit_now_check_in', 
	   'get_fit_now_member',
	   'income',
	   'interview',
	   'person',
	   'solution'
   )
ORDER BY table_name;
```
We can save this query somewhere and comment out the tables, except for the ones we need for a given step.

---

Ok, now looking at the intro text, we can see that there are a couple clues we can use for our first investigative query:

* Crime type: murder
* Date: Jan 15 2018, formatted as integer: `20180115`
* City: SQL City

Using this information we can construct the following query to find our crime scene report:

```sql
SELECT * FROM crime_scene_report 
	WHERE date = 20180115 and 
	type = 'murder' and 
	city = 'SQL City';

```

This query produces the following result:

| date 	| type 	| description 	| city 	|
|---	|---	|---	|---	|
| 20180115 	| murder	| Security footage shows that there were 2 witnesses. The first witness lives at the last house on **"Northwestern Dr"**. The second witness, named Annabel, lives somewhere on **"Franklin Ave"**. | SQL City	|

Interesting... We need to find their statements in the `interview` table, but the key is `person_id`. 
We need to find the `person_id` for both our witnesses to be able to get their recorded statements.

Let's find the first witness' name and ID. We know they live on **Northwestern Dr** and it's the last house.
Using both these clues we can find the first witness in the `person` table with the following query:

```sql
SELECT * FROM person 
	WHERE address_street_name = 'Northwestern Dr'
	ORDER BY address_number DESC
	LIMIT 1;
```

This query produces the following result:

| id | name | license_id | address_number | address_street_name | ssn |
|---|---|----------|---------------------------------------------|-------------------|---------|
| 14887 | Morty Schapiro | 118009 | 4919 | Northwestern Dr | 111564949 |

Let's find the ID for Annabel:

```sql
SELECT * FROM person 
	WHERE name LIKE 'Annabel%' AND
	address_street_name = 'Franklin Ave'
```

| id 	| name 	| license_id 	| address_number 	| address_street_name 	| ssn 	|
|---	|---	|---	|---	|---	|---	|
| 16371 	| Annabel Miller 	| 490173 	| 103 	| Franklin Ave 	| 318771143 	|

Great! Since we now have the `id` for both our witnesses, let's see what there statements are:

```sql
SELECT * FROM interview
	WHERE person_id IN (14887, 16371)
```

| person_id 	| transcript 	|
|---	|---	|
| 14887 	| I heard a gunshot and then saw a man run out. He had a "Get Fit Now Gym" bag. The membership number on the bag started with "48Z". Only gold members have those bags. The man got into a car with a plate that included "H42W". 	|
| 16371 	| I saw the murder happen, and I recognized the killer from my gym when I was working out last week on January the 9th. 	|

So we now know that the the killer is a member of the gym 'Get Fit Now'. Luckily, we have received the member and check-in data from this gym.
First lets corroborate that they saw the same person:

```sql
SELECT member.id, member.name, member.membership_status, check_in.check_in_date, check_in.check_in_time, check_in.check_out_time  
	FROM get_fit_now_member member 
	JOIN get_fit_now_check_in check_in ON check_in.membership_id = member.id 
	WHERE membership_status = 'gold' and 
		check_in.check_in_date = 20180109;
```

This query produces the following result:

| id 	| name 	| membership_status 	| check_in_date 	| check_in_time 	| check_out_time 	|
|---	|---	|---	|---	|---	|---	|
| XTE42 	| Sarita Bartosh 	| gold 	| 20180109 	| 486 	| 1124 	|
| 6LSTG 	| Burton Grippe 	| gold 	| 20180109 	| 399 	| 515 	|
| GE5Q8 	| Carmen Dimick 	| gold 	| 20180109 	| 367 	| 959 	|
| 48Z7A 	| Joe Germuska 	| gold 	| 20180109 	| 1600 	| 1730 	|
| 48Z55 	| Jeremy Bowers 	| gold 	| 20180109 	| 1530 	| 1700 	|
| 90081 	| Annabel Miller 	| gold 	| 20180109 	| 1600 	| 1700 	|

We can see here that there are two members with an id starting with `48Z` that were around the same time as our witness Annabel Miller.

Let's take a look at that partial license plate:

```sql
SELECT * FROM drivers_license dl JOIN person p ON dl.id = p.license_id WHERE plate_number LIKE '%H42W%';
```

Result:

| id 	| age 	| height 	| eye_color 	| hair_color 	| gender 	| plate_number 	| car_make 	| car_model 	| id-2 	| name 	| license_id 	| address_number 	| address_street_name 	| ssn 	|
|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|
| 664760 	| 21 	| 71 	| black 	| black 	| male 	| 4H42WR 	| Nissan 	| Altima 	| 51739 	| Tushar Chandra 	| 664760 	| 312 	| Phi St 	| 137882671 	|
| 423327 	| 30 	| 70 	| brown 	| brown 	| male 	| 0H42W2 	| Chevrolet 	| Spark LS 	| 67318 	| Jeremy Bowers 	| 423327 	| 530 	| Washington Pl, Apt 3A 	| 871539279 	|
| 183779 	| 21 	| 65 	| blue 	| blonde 	| female 	| H42W0X 	| Toyota 	| Prius 	| 78193 	| Maxine Whitely 	| 183779 	| 110 	| Fisk Rd 	| 137882671 	|

Here we see Jemery Bowers, the name name we also saw in the previous results. Let's see what he has to say:

```sql
SELECT * FROM interview WHERE person_id = 67318;
```

This query produces the following result:

| person_id 	| transcript 	|
|---	|---	|
| 67318 	| I was hired by a woman with a lot of money. I don't know her name but I know she's around 5'5" (65") or 5'7" (67"). She has red hair and she drives a Tesla Model S. I know that she attended the SQL Symphony Concert 3 times in December 2017. 	|

Interesting! We have a facebook events table, let's see if the event is there:

```sql
SELECT person_id, count(person_id) as c FROM facebook_event_checkin 
	WHERE event_name = 'SQL Symphony Concert' and
	date BETWEEN 20171201 and 20171231
	GROUP BY person_id
	HAVING count(person_id) = 3
```

This query produces the following result:

| person_id 	| times_attended 	|
|---	|---	|
| 24556 	| 3 	|
| 99716 	| 3 	|

Only 2 persons have been 3 times to the concert in December 2017.

Lets see what car they drive:

```sql
SELECT * FROM person p JOIN drivers_license dl ON p.license_id = dl.id
	WHERE p.id IN (24556, 99716);
```

| id 	| name 	| license_id 	| address_number 	| address_street_name 	| ssn 	| id-2 	| age 	| height 	| eye_color 	| hair_color 	| gender 	| plate_number 	| car_make 	| car_model 	|
|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|---	|
| 99716 	| Miranda Priestly 	| 202298 	| 1883 	| Golden Ave 	| 987756388 	| 202298 	| 68 	| 66 	| green 	| red 	| female 	| 500123 	| Tesla 	| Model S 	|

Bingo! Miranda Priestly is our prime suspect. You can check that this is the correct answer here: https://mystery.knightlab.com/, under "Check your solution".
